const conn = require('../config/database')
const {
    responseMessageFail,
    responseError
} = require('../utils/responseHandler')

// Create User Profile
exports.createUser = (response, data, callback) => {
    let query = 'INSERT INTO users SET ?'

    conn.query(query, data, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed to Create User Profile')
        return callback(rows)
    })
}

// Read One User Profile
exports.getUser = (response, filter, callback) => {
    let query = 'SELECT * FROM users WHERE id = ?'

    conn.query(query, filter, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed to Get User Profile')
        return callback(...rows)
    });
};

// Update Profile
exports.updateUser = (response, filter, data, callback) => {
    let querySearch = 'SELECT * FROM users WHERE id = ?'
    let queryUpdate = 'UPDATE users SET ? WHERE id = ?'

    conn.query(querySearch, filter, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed to Get User Profile')

        if (!rows.length) return responseMessageFail(response, 500, 'Profile Not Found!')

        else conn.query(queryUpdate, [data, filter], (err) => {
            if (err) return responseError(response, 500, err.message, 'Failed to Update User Profile')
            return callback(...rows)
        })
    })
}