const conn = require('../config/database')
const {
    responseError
} = require('../utils/responseHandler')

// Create User Read News
exports.createUserReadNews = (response, data) => {
    let query = 'INSERT INTO user_read_news SET ?'

    conn.query(query, data, (err) => {
        if (err) return responseError(response, 500, err, 'Failed Create Log Reading News')
    })
}

// Check Read User News
exports.checkUserReadNews = (response,news_id, user_id, callback) => {
    let query = `SELECT id FROM user_read_news WHERE news_id = ${news_id} && user_id = ${user_id}`;

    conn.query(query, (err, rows) => {
        if (err) return responseError(response, 500, err, 'Failed Get News')

        return callback(rows);
    });
};