const conn = require('../config/database')
const {
    responseError
} = require('../utils/responseHandler')

// Create User Authentication
exports.createUserAuth = (response, data, callback) => {
    let query = 'INSERT INTO user_authentications SET ?'

    conn.query(query, data, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed to Get User Authentication')
        return callback(rows)
    })
}