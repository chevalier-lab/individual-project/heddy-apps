const conn = require('../config/database');
const {
    responseData,
    // responseMessageSuccess,
    // responseMessageFail,
    responseError
} = require('../utils/responseHandler')

// Create News
exports.createNews = (response, data, callback) => {
    let query = 'INSERT INTO news SET ?';

    conn.query(query, data, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Create News!')
        return callback(rows.insertId);
    });
}

// Read All News
// exports.readNews = (response) => {
//     let query = 'SELECT * FROM news';

//     conn.query(query, (err, rows, field) => {
//         if (err) return responseError(response, 500, err.message, 'Fail Get News')
//         return responseData(response, 200, rows);
//     });
// };

// Read One News
// exports.readOneNews = (response, id) => {
//     let query = 'SELECT * FROM news WHERE id = ?';

//     conn.query(query, id, (err, rows, field) => {
//         if (err) return responseError(response, 500, err.message, 'Failed Get News')

//         if (!rows.length) return responseMessageFail(response, 404, 'ID Not Found');

//         return responseData(response, 200, rows);
//     });
// };

// Update News
// exports.updateNews = (response, id, data) => {
//     let querySearch = 'SELECT * FROM news WHERE id = ?';
//     let queryUpdate = 'UPDATE news SET ? WHERE id = ?';

//     conn.query(querySearch, id, (err, rows, field) => {
//         if (err) return responseError(response, 500, err.message, 'Failed Get News')

//         if (!rows.length)
//             return responseMessageFail(response, 404, 'News Not Found!');

//         else conn.query(queryUpdate, [data, id], (err, rows, field) => {
//             if (err) return responseError(response, 500, err.message, 'Failed Update News')
//             return responseMessageSuccess(response, 200, 'Success Update News!');
//         });
//     });
// };

// Delete News
// exports.deleteNews = (response, id) => {
//     let querySearch = 'SELECT * FROM news WHERE id = ?';
//     let queryDelete = 'DELETE FROM news WHERE id = ?';

//     conn.query(querySearch, id, (err, rows, field) => {
//         if (err) return responseError(response, 500, err.message, 'Failed Get News')

//         if (!rows.length) {
//             return responseMessageFail(response, 404, 'News Not Found!');
//         } else {
//             conn.query(queryDelete, id, (err, rows, field) => {
//                 if (err) return responseError(response, 500, err.message, 'Failed Delete News')

//                 return responseMessageSuccess(response, 200, 'Success Delete News!');
//             });
//         }
//     });
// };

// Get One News By Title
exports.getNewsByTitle = (response, author, title, callback) => {
    let query = `SELECT id FROM news WHERE author = '${author}' && title = '${title}'`;

    conn.query(query, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Get News')
        if (rows.length == 0) {
            return callback(false);
        }
        return callback(rows[0].id);
    });
};

// Get All News with User ID
exports.getAllNews = (response,user_id) => {
    let query = `
    SELECT news.*, medias.url, user_read_news.user_id
    FROM news
    JOIN medias ON news.cover_id = medias.id
    JOIN user_read_news ON news.id = user_read_news.news_id
    WHERE user_id = ${user_id};
    `

    conn.query(query, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Fail Get News')
        return responseData(response, 200, rows);
    });
};