const conn = require('../config/database')
const {
    responseMessageSuccess,
    responseError,
    responseData,
    responseMessageFail
} = require('../utils/responseHandler')

// Create Log Mineral Water Consumptions
exports.createMineralWaterConsumptions = (response, id, qty) => {
    let query = `INSERT INTO mineral_water_consumptions(user_activity_id, qty) VALUES(${id}, ${qty})`

    conn.query(query, (err) => {
        if (err) return responseError(response, 500, err.message, 'Failed Create Log Mineral Water Consumptions')
        return responseMessageSuccess(response, 201, 'Success Create Create Log Mineral Water Consumptions')
    })
}

// Read Log Mineral Water Consumptions
// exports.readMineralWaterConsumptions = (response, id, date, callback) => {
//     let query = `SELECT SUM(qty) AS sum FROM mineral_water_consumptions WHERE user_activity_id = ${id} AND timestamp LIKE '${date}%'`

//     conn.query(query, (err, rows) => {
//         if (err) return responseError(response, 500, err, 'Failed Read Log Mineral Water Consumptions')

//         return callback(rows[0].sum);
//     })
// }

// Read Sum Mineral Water Consumptions with The Maximum
exports.readMineralWaterConsumptionsWithMax = (response, id, date, callback) => {
    let query = `
    SELECT SUM(qty) AS sum, mineral_water_consumption AS max 
    FROM mineral_water_consumptions 
    JOIN user_activities ON user_activities.id = mineral_water_consumptions.user_activity_id
    WHERE user_activity_id = ${id} AND timestamp LIKE '${date}%'
    `

    conn.query(query, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Read Sum Mineral Water Consumptions with Max')

        return callback(rows[0].sum, rows[0].max);
    })
}

// Read Mineral Water Consumptions History
exports.getMineralWaterConsumptionsHistoryToday = (response, id, callback) => {
    let rawDate = new Date();
    let date = rawDate.getDate();
    if (date < 10) {
        date = `0${date}`;
    }
    let month = rawDate.getMonth() + 1;
    if (month < 10) {
        month = `0${month}`;
    }
    const year = rawDate.getFullYear();
    let query = `SELECT * FROM mineral_water_consumptions WHERE user_activity_id = ${id} AND timestamp LIKE '${year}-${month}-${date}%' ORDER BY id DESC`

    conn.query(query, (err, rows) => {
        if (err) return responseError(response, 500, err, 'Failed Read Mineral Water Consumptions History')

        if (!rows.length) return responseMessageFail(response, 500, 'Mineral Water Consumptions History Not Found')

        return responseData(response, 200, rows);
    })
}

// Read All Total Mineral Water Consumptions
exports.getMineralWaterConsumptionsAllTotal = (response, id) => {
    let rawDate = new Date();
    // This Day
    let date = rawDate.getDate();
    if (date < 10) {
        date = `0${date}`;
    }
    let month = rawDate.getMonth() + 1;
    if (month < 10) {
        month = `0${month}`;
    }
    const year = rawDate.getFullYear();
    // This Week
    function getStartWeek(date) {
        let rawDate = new Date(date.setDate(date.getDate() - date.getDay() + (date.getDay() === 0 ? -6 : 1)));
        const getStart = {
            day: rawDate.getDate(),
            month: rawDate.getMonth() + 1,
            year: rawDate.getFullYear()
        }
        return getStart;
    }

    function getEndWeek(date) {
        let rawDate = new Date(date.setDate(date.getDate() - date.getDay() + (date.getDay() === 0 ? -6 : 1) + 7));
        const getEnd = {
            day: rawDate.getDate(),
            month: rawDate.getMonth() + 1,
            year: rawDate.getFullYear()
        }
        return getEnd;
    }

    const startWeek = getStartWeek(new Date());
    const endWeek = getEndWeek(new Date());
    let query = `SELECT SUM(qty) AS daily FROM mineral_water_consumptions WHERE user_activity_id = ${id} AND timestamp LIKE '${year}-${month}-${date}%';`
    query += `Select SUM(qty) AS weekly FROM mineral_water_consumptions WHERE user_activity_id = ${id} AND timestamp >= '${startWeek.year}-${startWeek.month}-${startWeek.day}' AND timestamp <= '${endWeek.year}-${endWeek.month}-${endWeek.day}';`
    query += `SELECT SUM(qty) AS monthly FROM mineral_water_consumptions WHERE user_activity_id = ${id} AND timestamp LIKE '${year}-${month}%';`
    query += `SELECT SUM(qty) AS yearly FROM mineral_water_consumptions WHERE user_activity_id = ${id} AND timestamp LIKE '${year}%'`
    conn.query(query, (err, rows) => {
        if (err) return responseError(response, 500, err, 'Failed Read Mineral Water Consumptions History')

        if (!rows.length) return responseMessageFail(response, 500, 'Mineral Water Consumptions History Not Found')

        const data = {
            daily: rows[0][0].daily == null ? 0 : rows[0][0].daily,
            weekly: rows[1][0].weekly == null ? 0 : rows[1][0].weekly,
            monthly: rows[2][0].monthly == null ? 0 : rows[2][0].monthly,
            yearly: rows[3][0].yearly == null ? 0 : rows[3][0].yearly
        }
        return responseData(response, 200, data);
    })
}

// Read History between date
exports.getMineralWaterConsumptionsHistoryBeetween = (response, id, startDate, endDate) => {

    endDate = new Date(endDate)
    const tomorrow = new Date(endDate.setDate(endDate.getDate() + 1))
    endDate = `${tomorrow.getFullYear()}-${tomorrow.getMonth()+1}-${tomorrow.getDate()}`
    console.log(endDate);
    let query = `Select * FROM mineral_water_consumptions WHERE user_activity_id = ${id} AND timestamp >= '${startDate}' AND timestamp <= '${endDate}';`
    query += `Select SUM(qty) AS total FROM mineral_water_consumptions WHERE user_activity_id = ${id} AND timestamp >= '${startDate}' AND timestamp <= '${endDate}';`

    conn.query(query, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Read Total Mineral Water Consumptions')

        if (!rows[0].length) return responseMessageFail(response, 500, 'Mineral Water Consumptions History Not Found')
        return response.status(200).json({success: true, sum: rows[1][0], data: rows[0]})
    })
}


// Read Total Mineral Water Consumptions Today
exports.readMineralWaterConsumptions = (response, id, callback) => {
    let query = `SELECT SUM(qty) AS sum FROM mineral_water_consumptions WHERE user_activity_id = ${id} AND timestamp LIKE '${date}%'`

    conn.query(query, (err, rows) => {
        if (err) return responseError(response, 500, err, 'Failed Read Total Mineral Water Consumptions')

        return callback(rows[0].sum);
    })
}