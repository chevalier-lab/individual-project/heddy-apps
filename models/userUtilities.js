const conn = require('../config/database')
const {
    responseMessageFail,
    responseError
} = require('../utils/responseHandler')

// Create User User Utilities
exports.createUserUtilities = (response, data, callback) => {
    let query = 'INSERT INTO user_utilities SET ?'

    conn.query(query, data, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Create User Utilities!')
        return callback(rows)
    })
}

// Read One User User Utilities
exports.getUserUtilities = (response, filter, callback) => {
    let query
    if (filter.type == 'email') query = 'SELECT * FROM user_utilities WHERE email = ?';
    else if (filter.type == 'token') query = 'SELECT * FROM user_utilities WHERE token = ?'
    else return responseMessageFail(response, 500, 'Filter Needed')

    conn.query(query, filter.data, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Get User Utilities')
        return callback(...rows);
    });
};