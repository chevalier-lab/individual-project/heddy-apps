const conn = require('../config/database')
const {
    responseError
} = require('../utils/responseHandler')

// Create Media
exports.createMedias = (response, data, callback) => {
    let query = 'INSERT INTO medias SET ?'

    conn.query(query, data, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Upload Media')

        return callback(rows);
    });
}

// Get Media
exports.getMedias = (response, filter, callback) => {
    const query = 'SELECT * FROM medias WHERE id = ?'

    conn.query(query, filter, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Get Media')

        return callback(...rows)
    })
}

// Update Media
exports.updateMedias = (response, filter, data, callback) => {
    let querySearch = 'SELECT * FROM medias WHERE id = ?'
    let queryUpdate = 'UPDATE medias SET ? WHERE id = ?'

    conn.query(querySearch, filter, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed to Get User Photo')

        if (!rows.length) return responseMessageFail(response, 500, 'Photo Not Found!')

        else conn.query(queryUpdate, [data, filter], (err) => {
            if (err) return responseError(response, 500, err.message, 'Failed to Update User Photo')
            return callback(...rows)
        })
    })
}

// Delete Media
exports.deleteMedias = (response, filter, callback) => {
    let querySearch = 'SELECT * FROM medias WHERE id = ?';
    let queryDelete = 'DELETE FROM medias WHERE id = ?';

    conn.query(querySearch, filter, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Get Facts')

        if (!rows.length) return responseMessageFail(response, 500, 'Facts Not Found!');
        else
            conn.query(queryDelete, filter, (err) => {
                if (err) return responseError(response, 500, err.message, 'Failed Delete Facts')
                return callback(...rows)
            })
    })
};