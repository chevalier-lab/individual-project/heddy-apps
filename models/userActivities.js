const conn = require('../config/database')
const {
    responseMessageFail,
    responseError
} = require('../utils/responseHandler')

// Create User User Activities
exports.createUserActivities = (response, data, callback) => {
    let query = 'INSERT INTO user_activities SET ?'

    conn.query(query, data, (err, rows) => {
        if (err) return responseMessageFail(response, 500, 'Failed Create User Activities!')
        return callback(rows)
    })
}

// Read One User User Activities
exports.getUserActivities = (response, filter, callback) => {
    let query = 'SELECT * FROM user_activities WHERE user_utility_id = ?'

    conn.query(query, filter, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Get User Activities')
        return callback(...rows)
    });
};

// Update User Activities
exports.updateUserActivities = (response, filter, data, callback) => {
    let querySearch = 'SELECT * FROM user_activities WHERE user_utility_id = ?'
    let queryUpdate = 'UPDATE user_activities SET ? WHERE user_utility_id = ?'

    conn.query(querySearch, filter, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Get User Activities')

        if (!rows.length) return responseMessageFail(response, 500, 'User Activities Not Found!')

        else conn.query(queryUpdate, [data, filter], (err) => {
            if (err) return responseError(response, 500, err.message, 'Failed Update User Activities!')

            return callback(...rows)
        })
    })
}

// Read Maximum Mineral Water
// exports.readMaximumMineralWater = (response, id, callback) => {
//     let query = 'SELECT mineral_water_consumption FROM user_activities WHERE id = ?';

//     conn.query(query, id, (err, rows, field) => {
//         if (err) return responseError(response, 500, err, 'Failed Get Maximum Mineral Water')

//         if (!rows.length) return response.status(404).json({message: 'User Activities Not Found'});
//         // responseMessage(response, 201, 'Berhasil membuat media!');
//         return callback(rows[0].mineral_water_consumption);
//     });
// }