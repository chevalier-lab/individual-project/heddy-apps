const conn = require('../config/database')
const {
    responseMessageFail,
    responseError
} = require('../utils/responseHandler')

// Create Fact
exports.createFacts = (response, data, callback) => {
    let query = 'INSERT INTO facts SET ?'

    conn.query(query, data, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Create Facts')

        return callback(rows);
    });
}

// Get Facts
exports.allFacts = (response, callback) => {
    const query = 'SELECT * FROM facts'

    conn.query(query, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Get Facts')

        return callback(rows)
    })
}

// Update Fact
exports.updateFacts = (response, filter, data, callback) => {
    let querySearch = 'SELECT * FROM facts WHERE id = ?'
    let queryUpdate = 'UPDATE facts SET ? WHERE id = ?'

    conn.query(querySearch, filter, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Get Facts')

        if (!rows.length) return responseMessageFail(response, 500, 'Facts not Found')

        else conn.query(queryUpdate, [data, filter], (err) => {
            if (err) return responseError(response, 500, err.message, 'Failed Update Facts')
            return callback(...rows)
        })
    })
}

// Delete Fact
exports.deleteFacts = (response, filter, callback) => {
    let querySearch = 'SELECT * FROM facts WHERE id = ?';
    let queryDelete = 'DELETE FROM facts WHERE id = ?';

    conn.query(querySearch, filter, (err, rows) => {
        if (err) return responseError(response, 500, err.message, 'Failed Get Facts')

        if (!rows.length) return responseMessageFail(response, 500, 'Facts Not Found!');
        else
            conn.query(queryDelete, filter, (err) => {
                if (err) return responseError(response, 500, err.message, 'Failed Delete Facts')
                return callback(...rows)
            })
    })
};