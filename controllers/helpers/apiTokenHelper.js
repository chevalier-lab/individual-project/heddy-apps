const {
    getUserUtilities
} = require('../../models/userUtilities')
const {
    responseMessageFail
} = require('../../utils/responseHandler')

exports.apiTokenHelper = (req, res, next) => {
    const token = req.header('api_token')

    if (!token) return responseMessageFail(res, 400, 'Token Needed')

    // Get User Utilities
    getUserUtilities(res, {
        type: 'token',
        data: token
    }, (result) => {
        if (result == null) return responseMessageFail(res, 404, 'Failed Get User Utilities')
        req.utils = result
        next()
    })
}