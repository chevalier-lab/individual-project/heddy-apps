const express = require('express')
const router = express.Router()
const {
    getUserActivities,
    updateUserActivities
} = require('../models/userActivities')
const {
    responseMessageFail,
    responseMessageSuccess,
    responseData
} = require('../utils/responseHandler')
const {
    apiTokenHelper
} = require('./helpers/apiTokenHelper')
const {
    userActivitiesValidation
} = require('../utils/validate')

// @desc    Get Single User Activities
// @route   GET /user-activities/get
// @access  Private
router.get('/get', apiTokenHelper, (req, res) => {

    getUserActivities(res, req.utils.id, (result) => {
        if (result == null) return responseMessageFail(res, 404, 'Failed Get User Activities')
        delete result.id
        delete result.user_utility_id
        return responseData(res, 200, result)
    })
})

// @desc    Update User Activities
// @route   PUT /user-activities/update
// @access  Private
router.put('/update', apiTokenHelper, (req, res) => {

    const data = {
        ...req.body
    }
    delete data.id
    delete data.user_utility_id

    const validateErrors = userActivitiesValidation(data)
    if (validateErrors) return responseMessageFail(res, 400, {
        ...validateErrors
    } ['0'])

    updateUserActivities(res, req.utils.id, data, (result) => {
        if (result == null) return responseMessageFail(res, 400, 'Failed Update User Activities')
        return responseMessageSuccess(res, 200, 'Success Update User Activities')
    })
})

module.exports = router