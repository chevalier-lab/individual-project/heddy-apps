const express = require('express')
const path = require('path')
const fs = require('fs')
const router = express.Router()
const {
    createFacts,
    allFacts,
    updateFacts,
    deleteFacts
} = require('../models/facts');
const {
    getMedias,
    createMedias,
    updateMedias,
    deleteMedias
} = require('../models/medias')
const {
    responseMessageSuccess,
    responseMessageFail,
    responseData
} = require('../utils/responseHandler')
const {
    apiTokenHelper
} = require('./helpers/apiTokenHelper')
const {
    factsValidation
} = require('../utils/validate')

// @desc    Get All Facts
// @route   GET /facts/get
// @access  Private
router.get('/all', apiTokenHelper, (req, res) => {

    allFacts(res, (result) => {
        return responseData(res, 200, result)
    })
});

// @desc    Create Facts
// @route   POST /facts/create
// @access  Public
router.post('/create', (req, res) => {

    const {
        title,
        description,
    } = req.body

    const data = {
        file: req.files,
        title,
        description
    }

    const validateErrors = factsValidation(data)
    if (validateErrors) return responseMessageFail(res, 400, {
        ...validateErrors
    } ['0'])

    var filetypes = /jpeg|jpg|png/;
    var mimetype = filetypes.test(data.file.uploaded_file.mimetype);
    var extname = filetypes.test(path.extname(data.file.uploaded_file.name).toLowerCase());
    if (!mimetype && !extname) {
        return responseMessageFail(res, 400, 'Upload File not an Image')
    }

    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
    const newFileName = `${uniqueSuffix}-${data.file.uploaded_file.name}`
    data.file.uploaded_file.mv(`${__dirname}/../public/fact/${newFileName}`, err => {
        if (err) return responseMessageFail(res, 400, err)

        createMedias(res, {
            url: `public/fact/${newFileName}`
        }, (result) => {
            if (result.affectedRows != 1) return responseMessageFail(res, 400, 'File Error')
            else {
                delete data.file
                data.icon_id = result.insertId;
                data.header_id = result.insertId
                createFacts(res, data, (result) => {
                    if (result.affectedRows != 1) return responseMessageFail(res, 400, 'Failed Update Fact')
                    return responseMessageSuccess(res, 200, 'Create Facts Success')
                });
            }
        });
    });
})

// @desc    Update Facts
// @route   PUT /facts/update/:id
// @access  Public
router.put('/update/:id', (req, res) => {

    const id = req.params.id;
    const req_body = {
        ...req.body
    }
    delete req_body.id

    const data = {
        file: req.files,
        input: req_body
    }

    // Update Facts
    updateFacts(res, id, data.input, (result) => {
        if (result == null) return responseMessageFail(res, 400, 'App Crash')
        if (data.file != null) {
            var filetypes = /jpeg|jpg|png/;
            var mimetype = filetypes.test(data.file.uploaded_file.mimetype);
            var extname = filetypes.test(path.extname(data.file.uploaded_file.name).toLowerCase());
            if (!mimetype && !extname) return responseMessageFail(res, 400, 'Upload File not an Image')

            const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
            const newFileName = `${uniqueSuffix}-${data.file.uploaded_file.name}`
            const newFilePath = `public/fact/${newFileName}`
            const filePath = `${__dirname}/../`

            // Upload File
            data.file.uploaded_file.mv(`${filePath}${newFilePath}`, err => {
                if (err) return responseMessageFail(res, 400, err)

                // Get Old Media
                getMedias(res, result.icon_id, (result) => {
                    if (result == null) return responseMessageFail(res, 400, 'App Crash')
                    const media_url = result.url

                    // Update Media
                    updateMedias(res, result.id, {
                        url: `${newFilePath}`
                    }, (result) => {
                        if (result == null) return responseMessageFail(res, 400, 'Failed Update User Photo')

                        // Check if file exist
                        if (fs.existsSync(`${filePath}${media_url}`)) {

                            // Remove File
                            fs.unlink(`${filePath}${media_url}`, (err) => {
                                if (err) return responseMessageFail(res, 400, 'App Crash')
                            })
                        }
                    })
                })
            });
        }
        return responseMessageSuccess(res, 200, 'Update Facts Success')
    })
})

// @desc    Delete Facts
// @route   DELETE /facts/delete
// @access  Public
router.delete('/delete/:id', (req, res) => {
    const id = req.params.id;

    deleteFacts(res, id, (result) => {
        if (result == null) return responseMessageFail(res, 400, 'App Crash')

        // Delete Media Row
        deleteMedias(res, result.icon_id, (result) => {
            if (result == null) return responseMessageFail(res, 400, 'App Crash')
            const filePath = `${__dirname}/../${result.url}`

            // Check if file exist
            if (fs.existsSync(filePath)) {

                // Remove File
                fs.unlink(filePath, (err) => {
                    if (err) return responseMessageFail(res, 400, 'App Crash')
                })
            }

            return responseMessageSuccess(res, 200, 'Delete Facts Success')
        })
    });
})


module.exports = router;