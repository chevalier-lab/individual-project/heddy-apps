const express = require('express')
const router = express.Router()
const validate = require("validate.js")
const {
    createMineralWaterConsumptions,
    readMineralWaterConsumptions,
    readMineralWaterConsumptionsWithMax,
    readMineralWaterConsumptionsHistory,
    getMineralWaterConsumptionsAllTotal,
    getMineralWaterConsumptionsHistoryBeetween,
    getMineralWaterConsumptionsHistoryToday
} = require('../models/mineralWaterConsumptions')
const {
    getUserActivities
} = require('../models/userActivities')
const {
    apiTokenHelper
} = require('./helpers/apiTokenHelper')

// @desc    Create Mineral Water Consumptions
// @route   POST /mineral_water_consumptions/create
// @access  Private
router.post('/create', apiTokenHelper, (req, res) => {
    getUserActivities(res, req.utils.id, function (userActivities) {
        let qty = req.body.qty;
        const today = new Date().toJSON().substring(0, 10);
        if (!validate.isNumber(qty)) {
            return res.status(403).json({
                message: 'Not A Valid Number!'
            })
        } else if (qty < 1) {
            return res.status(403).json({
                message: 'Minimum quantity 1!'
            })
        } else {
            readMineralWaterConsumptionsWithMax(res, userActivities.id, today, function (sum, max) {
                if (sum == max) {
                    return res.status(403).json({
                        message: 'You have reached the maximum limit!'
                    })
                }
                const add = qty + sum;
                if (add >= max) {
                    // return res.status(403).json({
                    //     message: 'You have reached the maximum limit!'
                    // })
                    qty = max - sum
                }
                createMineralWaterConsumptions(res, userActivities.id, qty)
            })
        }
    })
})

// @desc    Read Mineral Water Consumptions Total
// @route   GET /mineral-water-consumptions/get-total
// @access  Private
router.get('/get-total', apiTokenHelper, (req, res) => {
    // readMineralWaterConsumptionsHistory(res, id, date);
    getUserActivities(res, req.utils.id, function (userActivities) {
        getMineralWaterConsumptionsAllTotal(res, userActivities.id)
    })

})

// @desc    Read Mineral Water Consumptions Today
// @route   GET /mineral-water-consumptions/get-today-history
// @access  Private
router.get('/get-today-history', apiTokenHelper, (req, res, ) => {
    getUserActivities(res, req.utils.id, function (userActivities) {
        getMineralWaterConsumptionsHistoryToday(res, userActivities.id)
    })
})

// @desc    Read Mineral Water Consumptions History & Total Between Date
// @route   GET /mineral-water-consumptions/get-between
// @access  Private
router.get('/get-between', apiTokenHelper, (req, res) => {
    const data = {
        ...req.body
    }
    getUserActivities(res, req.utils.id, function (userActivities) {
        getMineralWaterConsumptionsHistoryBeetween(res, userActivities.id, data.startDate, data.endDate)
    })
})

// @desc    Read Mineral Water Consumptions History
// @route   GET /mineral-water-consumptions/get/:id
// @access  Private
// router.get('/get/:id', (req, res) => {
//     const id = req.params.id;
//     const data = {
//         ...req.body
//     }
    // readMineralWaterConsumptionsHistory(res, id, date);
    // readMineralWaterConsumptionsHistoryBeetween(res, id, data.start, data.end)
// })

module.exports = router