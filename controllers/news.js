const express = require('express');
const router = express.Router();
const validate = require('validate.js');
const {
    createNews,
    // readNews,
    // readOneNews,
    // updateNews,
    // deleteNews,
    getNewsByTitle,
    getAllNews
} = require('../models/news');
const {
    checkUserReadNews,
    createUserReadNews
} = require('../models/userReadNews')
const {
    createMedias
} = require('../models/medias');
const NewsAPI = require('newsapi');
const newsapi = new NewsAPI('b4961a393cfe436697c11c0d762a93b9');

// @desc    Read News From API
// @route   GET /news/get-api/:search/:page
// @access  Public
router.get('/get-api/:search/:page', (req, res) => {
    newsapi.v2
        .everything({
            q: req.params.search,
            language: 'id',
            page: req.params.page,
        })
        .then((response) => {
            let data = [];
            let meta = {
                currentPage: parseInt(req.params.page),
                totalPage: Math.ceil(response.totalResults / 20),
                currentResult: response.articles.length,
                totalResult: response.totalResults,
            };
            response.articles.forEach((element) => {
                data.push({
                    cover_url: element.urlToImage,
                    title: element.title,
                    source: element.source.name,
                    author: element.author,
                    description: element.description,
                    content: element.content,
                    url: element.url,
                    timestamp: element.publishedAt
                });
            });
            res.status(200).json({
                data: data,
                meta: meta,
            });
        });
});

// @desc    Check & Create News to Database After Click News from NewsAPI
// @route   POST /news/create/:user_id
// @access  Private
router.post('/create/:user_id', (req, res) => {
    const userId = req.params.user_id;
    const data = {
        ...req.body
    }
    let endOfResponse = {};

    validate.validators.presence.message = 'is required';
    const error = validate(data, {
        cover_url: {
            presence: {
                allowEmpty: false
            },
            url: true
        },
        title: {
            presence: {
                allowEmpty: false
            }
        },
        source: {
            presence: {
                allowEmpty: false
            }
        },
        description: {
            presence: {
                allowEmpty: false
            }
        },
        url: {
            presence: {
                allowEmpty: false
            },
            url: true
        },
        timestamp: {
            presence: {
                allowEmpty: false
            }
        }
    });

    if (error) {
        return res.status(403).json(error);
    }
    if (data.author == null) {
        data.author = 'Heddy.ID';
    }
    if (data.content == null) {
        data.content = data.description;
    }
    getNewsByTitle(res, data.author, data.title, function (checkNews) {
        if (checkNews == false) {
            createMedias(res, {
                url: data.cover_url
            }, function (cover) {
                delete data['cover_url'];
                data.cover_id = cover.insertId;
                createNews(res, data, function (newsId) {
                    endOfResponse.createNews = true
                    checkUserReadNews(res, newsId, userId, function (result) {
                        if (result.length == 0) {
                            endOfResponse.statusRead = false;
                            createUserReadNews(res, {
                                news_id: newsId,
                                user_id: userId
                            })
                        } else {
                            endOfResponse.statusRead = true;
                        }
                        res.status(200).json(endOfResponse)
                    })
                });
            })
        } else {
            endOfResponse.createNews = false
            checkUserReadNews(res, checkNews, userId, function (result) {
                if (result.length == 0) {
                    endOfResponse.statusRead = false;
                    createUserReadNews(res, {
                        news_id: checkNews,
                        user_id: userId
                    })
                } else {
                    endOfResponse.statusRead = true;
                }
                res.status(200).json(endOfResponse)
            })
        }
    })
});

// @desc    Read All News with User ID
// @route   GET /news/read
// @access  Public
router.get('/get-all/:user_id', (req, res) => {
    const userId = req.params.user_id;
    getAllNews(res, userId)
});


// router.get('/read', (req, res) => {
//   readNews(res);
// });

// @desc    Read One News
// @route   GET /news/read/:id
// @access  Public
// router.get('/read/:id', (req, res) => {
//   readOneNews(res, req.params.id);
// });

// @desc    Update News
// @route   PUT /news/update/:id
// @access  Private
// router.put('/update/:id', (req, res) => {
//   const data = {
//     ...req.body,
//   };
//   const id = req.params.id;

//   updateNews(res, id, data);
// });

// @desc    Delete News
// @route   DELETE /news/delete/:id
// @access  Private
// router.delete('/delete/:id', (req, res) => {
//   const id = req.params.id;

//   deleteNews(res, id);
// });

module.exports = router;