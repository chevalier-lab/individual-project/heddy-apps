const express = require('express')
const sha1 = require('sha1')
const router = express.Router()
const {
    createUser,
    getUser
} = require('../models/users')
const {
    createUserUtilities,
    getUserUtilities
} = require('../models/userUtilities')
const {
    createUserAuth
} = require('../models/userAuthentications')
const {
    createMedias
} = require('../models/medias')
const {
    createUserActivities
} = require('../models/userActivities')
const {
    responseMessageFail,
    responseMessageSuccess
} = require('../utils/responseHandler')

// @desc    Create Authentication
// @route   POST /authentication/create
// @access  Public
router.post('/create', (req, res) => {

    const {
        uid,
        name,
        email,
        photo_url
    } = req.body

    const data = {
        uid,
        name,
        email,
        photo_url
    }

    // Check if email exist in user_utilities
    getUserUtilities(res, {
        type: 'email',
        data: data.email
    }, (result) => {
        if (result == null) {
            // Create User Profile
            const date = new Date()
            const user_data = {
                name,
                date_of_birth: `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`
            }
            createUser(res, user_data, (result) => {
                if (result.affectedRows != 1) return responseMessageFail(res, 400, 'Failed Create User Profile')
                else {

                    // Create Media / Face URL
                    const user_id = result.insertId
                    const media = {
                        url: data.photo_url
                    }
                    createMedias(res, media, (result) => {
                        if (result.affectedRows != 1) return responseMessageFail(res, 400, 'Fail Uploading User Image')

                        // Create User Utilities
                        const user_utilities = {
                            user_id: user_id,
                            face_id: result.insertId,
                            token: `${sha1(email + new Date().toISOString().replace(/T|Z/g, ' '))}`,
                            email
                        }
                        createUserUtilities(res, user_utilities, (result) => {
                            if (result.affectedRows != 1) return responseMessageFail(res, 400, 'Failed Create User Utilities')

                            // Create User Activities
                            const user_activities = {
                                user_utility_id: result.insertId
                            }

                            createUserActivities(res, user_activities, (result) => {
                                if (result.affectedRows != 1) return responseMessageFail(res, 400, 'Failed Create User Activities')
                                getUserUtilities(res, {
                                    type: 'email',
                                    data: data.email
                                }, (result) => {
                                    if (!result) return responseMessageFail(res, 400, 'Failed Authentication User')
                                    const token = result.token
                                    // Create Auth Log
                                    let key = result
                                    const auth_data = {
                                        user_utility_id: key.id,
                                        uid: data.uid
                                    }
                                    createUserAuth(res, auth_data, (result) => {
                                        if (result.affectedRows != 1) return responseMessageFail(res, 400, 'Failed to Sign Up')
                                        return responseMessageSuccess(res, 200, {
                                            res: 'User Profile not Done Yet',
                                            token: token
                                        })
                                    })
                                })
                            })
                        })
                    })
                }
            })
        } else {

            // Create Auth Log
            let key = result
            const auth_data = {
                user_utility_id: key.id,
                uid: data.uid
            }
            createUserAuth(res, auth_data, (result) => {
                if (result.affectedRows != 1) return responseMessageFail(res, 400, 'Failed to Sign in')
                else {
                    getUser(res, key.user_id, (result) => {
                        if ((result.height == null || result.height == 0) ||
                            (result.weight == null || result.weight == 0) ||
                            (result.activity_level == null || result.activity_level == 0)) return responseMessageSuccess(res, 200, {
                            res: 'User Profile not Done Yet',
                            token: key.token
                        })
                        else return responseMessageSuccess(res, 200, {
                            res: 'User Profile Complete',
                            token: key.token
                        })
                    })
                }
            })
        }
    })
})

module.exports = router