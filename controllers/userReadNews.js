const express = require('express')
const router = express.Router()
const {
    createUserReadNews,
} = require('../models/userReadNews')

// @desc    Create User Authentication
// @route   POST /read-news/create
// @access  Private
// router.post('/create', (req, res) => {
//     let data = {
//         ...req.body
//     }
//     createUserReadNews(res, data)
// })

module.exports = router