const express = require('express')
const path = require('path')
const fs = require('fs')
const router = express.Router()
const {
    updateUser,
    getUser
} = require('../models/users')
const {
    getMedias,
    updateMedias,
} = require('../models/medias')
const {
    responseMessageSuccess,
    responseMessageFail,
    responseData
} = require('../utils/responseHandler')
const {
    apiTokenHelper
} = require('./helpers/apiTokenHelper')
const {
    userValidation
} = require('../utils/validate')

// @desc    Get Single User Profile
// @route   GET /users/get
// @access  Private
router.get('/get', apiTokenHelper, (req, res) => {

    let data, utility, user_activities, user_medias, user_id = req.utils.user_id,
        face_id = req.utils.face_id,
        user_utilities = {
            token: req.utils.token,
            email: req.utils.email
        }

    // Get User Profile
    getUser(res, user_id, (result) => {
        if (result == null) return responseMessageFail(res, 400, 'Failed Get User Activities')
        delete result.id
        delete result.user_utility_id
        user_activities = result

        // Get User Photo
        getMedias(res, face_id, (result) => {
            if (result == null) return responseMessageFail(res, 400, 'Failed Get User Photo')
            user_medias = {
                face_url: result.url
            }
            utility = {
                utility: Object.assign(user_utilities, user_medias)
            }
            data = {
                ...user_activities,
                ...utility
            }

            return responseData(res, 200, data)
        })
    })
})

// @desc    Update User Profile
// @route   PUT /users/update
// @access  Private
router.put('/update', apiTokenHelper, (req, res) => {

    const req_body = {
        ...req.body
    }
    delete req_body.id

    const data = {
        file: req.files,
        input: req_body
    }

    const user_utilities = {
        user_id: req.utils.user_id,
        face_id: req.utils.face_id
    }

    const validateErrors = userValidation(data.input)
    if (validateErrors) return responseMessageFail(res, 400, {
        ...validateErrors
    } ['0'])

    // Update User
    updateUser(res, user_utilities.user_id, data.input, (result) => {
        if (result == null) return responseMessageFail(res, 400, 'Failed Update User Profile')
        if (data.file != null) {
            var filetypes = /jpeg|jpg|png/;
            var mimetype = filetypes.test(data.file.uploaded_file.mimetype);
            var extname = filetypes.test(path.extname(data.file.uploaded_file.name).toLowerCase());
            if (!mimetype && !extname) return responseMessageFail(res, 400, 'Upload File not an Image')

            const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
            const newFileName = `${uniqueSuffix}-${data.file.uploaded_file.name}`
            const newFilePath = `public/profile/${newFileName}`
            const filePath = `${__dirname}/../`

            // Upload File
            data.file.uploaded_file.mv(`${filePath}${newFilePath}`, err => {
                if (err) return responseMessageFail(res, 400, err)

                // Get Old Media Data
                getMedias(res, user_utilities.face_id, (result) => {
                    if (result == null) return responseMessageFail(res, 400, 'App Crash')
                    const media_url = result.url

                    // Update New Media
                    updateMedias(res, result.id, {
                        url: `${newFilePath}`
                    }, (result) => {
                        if (result == null) return responseMessageFail(res, 400, 'Failed Update User Photo')

                        // Check File Exist
                        if (fs.existsSync(`${filePath}${media_url}`)) {

                            // Remove File
                            fs.unlink(`${filePath}${media_url}`, (err) => {
                                if (err) return responseMessageFail(res, 400, 'App Crash')
                            })
                        }
                    })
                })
            });
        }
        return responseMessageSuccess(res, 200, 'Success Update User Profile')
    })
})

module.exports = router