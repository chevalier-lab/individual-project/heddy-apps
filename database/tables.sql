/** TABLE > USERS **/
CREATE TABLE IF NOT EXISTS users (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    gender TINYINT(1) DEFAULT 0,
    date_of_birth DATE NOT NULL,
    height TINYINT(3) UNSIGNED DEFAULT 0, 
    weight TINYINT(3) UNSIGNED DEFAULT 0,
    sleep_time VARCHAR(15) DEFAULT '22:00 - 06:00',
    activity_level TINYINT(1) DEFAULT 0,
    PRIMARY KEY pk_id_users(id)
);

/** TABLE > MEDIAS **/
CREATE TABLE IF NOT EXISTS `medias` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` TEXT,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY `pk_id_medias`(`id`)
) ENGINE = InnoDB;

/** TABLE > USERS > USER UTILITIES **/
CREATE TABLE IF NOT EXISTS user_utilities (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_id INT UNSIGNED NOT NULL,
    face_id INT UNSIGNED NOT NULL,
    token VARCHAR(255) NOT NULL,
    email VARCHAR(100) NOT NULL,
    CONSTRAINT fk_user_id_user_utilities FOREIGN KEY (user_id) REFERENCES users(id),
    CONSTRAINT fk_face_id_user_utilities FOREIGN KEY (face_id) REFERENCES medias(id)
);

/** TABLE > USERS > USER UTILITIES > USER AUTHENTICATIONS **/
CREATE TABLE IF NOT EXISTS user_authentications(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_utility_id INT UNSIGNED NOT NULL,
    uid INT NOT NULL,
    timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT fk_user_utitlity_id_user_authentications FOREIGN KEY (user_utility_id) REFERENCES user_utilities(id)
);

/** TABLE > USERS > USER UTILITIES > USER ACTIVITIES **/
CREATE TABLE user_activities (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_utility_id INT UNSIGNED NOT NULL,
    mineral_water_consumption INT NOT NULL DEFAULT 0,
    calories INT NOT NULL DEFAULT 0,
    sleep_duration INT NOT NULL DEFAULT 0,
    calorie_foods INT NOT NULL DEFAULT 0,
    steps INT NOT NULL DEFAULT 0
);

/** TABLE > USERS > USER UTILITIES > USER ACTIVITIES > MINERAL WATER CONSUMPTIONS **/
CREATE TABLE mineral_water_consumptions (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_activity_id INT UNSIGNED NOT NULL,
    qty INT NOT NULL,
    timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);


/** TABLE > FACTS **/
CREATE TABLE IF NOT EXISTS `facts` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `icon_id` INT UNSIGNED NOT NULL,
  `header_id` INT UNSIGNED NOT NULL,
  `title` TEXT NOT NULL,
  `description` TEXT NOT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY `pk_id_facts`(`id`)
) ENGINE = InnoDB;

/** TABLE > NEWS **/
CREATE TABLE IF NOT EXISTS `news` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cover_id` INT UNSIGNED NOT NULL,
  `title` TEXT NOT NULL,
  `source` VARCHAR(255) NOT NULL,
  `author` VARCHAR(50) NOT NULL,
  `description` TEXT NULL,
  `content` TEXT NULL,
  `url` TEXT NOT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY `pk_id_news`(`id`)
) ENGINE = InnoDB;

/** TABLE > NEWS > USER READ NEWS **/
CREATE TABLE IF NOT EXISTS `user_read_news` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `news_id` INT UNSIGNED NOT NULL,
  `user_id` INT UNSIGNED NOT NULL,  
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY `pk_id_user_read_news`(`id`)
) ENGINE = InnoDB;

/** ADD FOREIGN KEY FOR FACTS **/
ALTER TABLE `facts`
ADD CONSTRAINT `fk_icon_id_facts`
FOREIGN KEY (`icon_id`) REFERENCES `medias`(`id`);

ALTER TABLE `facts`
ADD CONSTRAINT `fk_header_id_facts`
FOREIGN KEY (`header_id`) REFERENCES `medias`(`id`);

/** ADD FOREIGN KEY FOR NEWS **/
ALTER TABLE `news`
ADD CONSTRAINT `fk_cover_id_news`
FOREIGN KEY (`cover_id`) REFERENCES `medias`(`id`);

/** ADD FOREIGN KEY FOR USER READ NEWS **/
ALTER TABLE `user_read_news`
ADD CONSTRAINT `fk_news_id_user_read_news`
FOREIGN KEY (`news_id`) REFERENCES `news`(`id`);

ALTER TABLE `user_read_news`
ADD CONSTRAINT `fk_user_id_user_read_news`
FOREIGN KEY (`user_id`) REFERENCES `users`(`id`);

/** ADD FOREIGN KEY FOR USER ACTIVITIES **/
ALTER TABLE user_activities ADD CONSTRAINT fk_user_utility_id_user_activities FOREIGN KEY (user_utility_id) REFERENCES user_utilities (id);

/** ADD FOREIGN KEY FOR MINERAL WATER CONSUMPTIONS **/
ALTER TABLE mineral_water_consumptions ADD CONSTRAINT fk_use_activities_id_mineral_water_consumptions FOREIGN KEY (user_activity_id) REFERENCES user_activities (id);
