exports.responseData = (response, statusCode, values) => {
    var data = {
        success: true,
        data: values
    }
    response.status(statusCode).json(data)
}

exports.responseMessageSuccess = (response, statusCode, message) => {
    var data = {
        success: true,
        message: message
    }
    response.status(statusCode).json(data)
}

exports.responseMessageFail = (response, statusCode, message) => {
    var data = {
        success: false,
        message: message
    }
    response.status(statusCode).json(data)
}

exports.responseError = (response, statusCode, err, message) => {
    var data = {
        error: err,
        message: message
    }
    response.status(statusCode).json(data)
}