var validate = require('validate.js')

exports.userValidation = attribute => {
    var constraints = {
        name: {
            length: {
                maximum: 100
            }
        },
        gender: {
            length: {
                maximum: 1
            },
            numericality: {
                onlyInteger: true,
                greaterThanOrEqualTo: 0,
                lessThan: 10,
                strict: true
            }
        },
        height: {
            length: {
                maximum: 3
            },
            numericality: {
                onlyInteger: true,
                greaterThanOrEqualTo: 0,
                lessThan: 1000,
                strict: true
            }
        },
        weight: {
            length: {
                maximum: 3
            },
            numericality: {
                onlyInteger: true,
                greaterThanOrEqualTo: 0,
                lessThan: 1000,
                strict: true
            }
        },
        activity_level: {
            length: {
                maximum: 1
            },
            numericality: {
                onlyInteger: true,
                greaterThanOrEqualTo: 0,
                lessThan: 10,
                strict: true
            }
        },
        file: {}
    }

    return validate(attribute, constraints, {
        format: 'flat'
    })
}

exports.userActivitiesValidation = attribute => {
    var constraints = {
        mineral_water_consumption: {
            numericality: {
                onlyInteger: true,
                greaterThanOrEqualTo: 0,
                strict: true
            }
        },
        calories: {
            numericality: {
                onlyInteger: true,
                greaterThanOrEqualTo: 0,
                strict: true
            }
        },
        sleep_duration: {
            numericality: {
                onlyInteger: true,
                greaterThanOrEqualTo: 0,
                strict: true
            }
        },
        calories_foods: {
            numericality: {
                onlyInteger: true,
                greaterThanOrEqualTo: 0,
                strict: true
            }
        },
        steps: {
            numericality: {
                onlyInteger: true,
                greaterThanOrEqualTo: 0,
                strict: true
            }
        },
    }

    return validate(attribute, constraints, {
        format: 'flat'
    })
}

exports.userUtilitiesValidation = attribute => {
    var constraints = {
        email: {
            presence: true,
            email: true
        }
    }

    return validate(attribute, constraints, {
        format: 'flat'
    })
}

exports.factsValidation = attribute => {
    var constraints = {
        file: {
            presence: true
        },
        title: {
            presence: true
        },
        description: {
            presence: true
        }
    }

    return validate(attribute, constraints, {
        format: 'flat'
    })
}