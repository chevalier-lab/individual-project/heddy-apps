const mysql = require('mysql');

//config mysql
const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'db_heddy',
    multipleStatements: true
});

//connect database
conn.connect((err) => {
    if (err) throw err;
    console.log("MySQL Connected...");
});

module.exports = conn;