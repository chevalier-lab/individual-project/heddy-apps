const express = require('express')
const fileUpload = require('express-fileupload')
const app = express()
const bodyParser = require('body-parser')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(fileUpload())

app.use('/authentication', require('./controllers/userAuthentications'))
app.use('/users', require('./controllers/users'))
app.use('/user-activities', require('./controllers/userActivities'))
app.use('/facts', require('./controllers/facts'))
app.use('/news', require('./controllers/news'))
app.use('/read-news', require('./controllers/userReadNews'))
app.use('/mineral-water-consumptions', require('./controllers/mineralWaterConsumptions'))

const PORT = process.env.PORT || 5000;

// Listening port
app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}!`)
});